package org.main.exceptions;

/**
 * Inappropriate Value Exception
 * 
 * @author Junshao Lin
 *
 */
public class InappropriateValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
