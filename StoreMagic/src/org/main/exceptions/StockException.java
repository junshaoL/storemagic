package org.main.exceptions;

/**
 * Stock Exception
 * Load sales log might trigger this exception
 * @author Junshao Lin
 *
 */
public class StockException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
