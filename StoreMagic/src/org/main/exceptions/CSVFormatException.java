package org.main.exceptions;

/**
 * CSV Format Exception
 * 
 * @author Junshao Lin
 *
 */
public class CSVFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
