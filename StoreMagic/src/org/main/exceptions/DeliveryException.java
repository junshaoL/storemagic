package org.main.exceptions;

/**
 * Delivery Exception
 * Load manifest log might trigger this exception
 * @author Junshao Lin
 *
 */
public class DeliveryException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
