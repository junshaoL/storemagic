package org.main.tools;

/**
 * Check the format of CSV if suitable for this program
 *
 */
public class CSVFormatChecker {
	
	private static boolean firstCheck; 
	
	private static boolean isInteger(String data) {
		try{
			Integer.parseInt(data);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	private static boolean isDouble(String data) {
		try{
			Double.parseDouble(data);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	public static void resetBooleanForManifest() {
		firstCheck = false;
	}
	
	/**
	 * Check if the data from manifest suitable for using
	 * @param data 
	 * @return the state
	 * 
	 * @author Junshao Lin
	 */
	public static boolean checkManifest(String data) {
		String[] splitData = data.split(",");
		
		if (splitData.length == 1) {
			if (splitData[0].equals(Constants.REFRIGERATED_NAME) || splitData[0].equals(Constants.ORDINARY_NAME)) {
				firstCheck = true;
				return true;
			}
		}
		
		if (firstCheck) {
			if (splitData.length != 2) {
				return false;
			}
			else if (isInteger(splitData[1])) {
				return true;
			}
		}		
		
		return false;
	}
	
	/**
	 * Check if the data from SalesLog suitable for using
	 * 
	 * @param data
	 * @return the state
	 * @author zq
	 */
	public static boolean checkSalesLog(String data) {
		String[] values = data.split(",");
		if (values.length != 2) {
			return false;
		}
		if(isInteger(values[1])) {
			return true;
		} 
		return false;
	}
	
	/**
	 * Check if the data from item properties suitable for using
	 * 
	 * @param data
	 * @return the state
	 * @author zq
	 */
	public static boolean checkProperties(String data) {
		String[] values = data.split(",");
		if (values.length > 6 | values.length < 5) {
			return false;
		}
		if (values.length == 5) {
			if(isDouble(values[1]) && isDouble(values[2]) && isInteger(values[3]) && isInteger(values[4])) {
				return true;
			} 
		}
		if (values.length == 6) {
			if(isDouble(values[1]) && isDouble(values[2]) && isInteger(values[3]) && isInteger(values[4]) && isInteger(values[5])) {
				return true;
			} 
		}
		return false;
	}
	
}
