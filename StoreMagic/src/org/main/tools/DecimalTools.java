package org.main.tools;

import java.text.DecimalFormat;

/**
 * Format double value to have 2 decimals
 * 
 * @author Junshao Lin
 *
 */
public class DecimalTools {
	
	private static DecimalFormat dformat = new DecimalFormat("#0.00");
	
	/**
	 * Foramt double
	 * @param value
	 * @return a string of formated double value
	 */
	public static String formDouble(double value) {
		return dformat.format(value);
	}
	
}
