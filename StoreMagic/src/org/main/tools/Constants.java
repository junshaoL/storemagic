package org.main.tools;

/**
 * Constants Class
 * 
 * Include most of the constants of this program
 * 
 * @author Junshao Lin
 *
 */
public final class Constants {
	public static final String REFRIGERATED_NAME = ">Refrigerated";
	public static final String ORDINARY_NAME = ">Ordinary";
	public static final String USERNAME = "admin";
	public static final String USERPASSWORD = "admin";
	public static final int TEMP_NO_NEED = 99;
	public static final double INITIAL_CAPITAL = 100000.0;
	public static final String WELCOME_MESSAGE = "Welcome to Inventory Manage System. ";
	public static final String REPLENISHMENT_MESSAGE = " items needs replenishing";
}
