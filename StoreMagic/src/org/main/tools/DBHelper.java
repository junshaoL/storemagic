package org.main.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;

import org.main.exceptions.InappropriateValueException;
import org.main.model.Item;

/**
 * Useful tools to connect with database
 * 
 * Make sure call commitChanges() after any changes
 * 
 * @author Junshao Lin
 *
 */
public class DBHelper {
	
	private static Connection connection;
	private static Statement statement;
	private static ResultSet resultSet;
	
	private static Connection getConnection() throws ClassNotFoundException, SQLException {
		if (connection == null) {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:super_market.db");
			connection.setAutoCommit(false);
		}
		
		return connection;
	}
	
	private static Statement getStatement() throws ClassNotFoundException, SQLException {
		if (statement == null) {
			statement = getConnection().createStatement();
		}
		return statement;
	}	
	
	/**
	 * Update a specific item in the database
	 * 
	 * @param propertyName				The name of which property of item will be changed
	 * @param itemName					The name of the item. This is used to identify which item will be operated 
	 * @param propertyType				The type of the property DATA_TYPE_DOUBLE DATA_TYPE_INT or DATA_TYPE_STRING
	 * @param propertyValue				The new value which will replace the old value in the database
	 *  
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static void updateInventory(String propertyName, String itemName, Object propertyValue) throws SQLException, ClassNotFoundException {
		Statement s = getStatement();
		String updateStatement = null;
		
		if (propertyValue instanceof String) {
			updateStatement = "UPDATE INVENTORY set " + propertyName + " = '" + 
					String.valueOf(propertyValue) + "' WHERE NAME = '" + itemName + "'";
		}
		else if (propertyValue instanceof Double) {
			updateStatement = "UPDATE INVENTORY set " + propertyName + " = " + 
					Double.valueOf(String.valueOf(propertyValue)) + " WHERE NAME = '" + itemName + "'";
		}
		else if (propertyValue instanceof Integer) {
			updateStatement = "UPDATE INVENTORY set " + propertyName + " = " + 
					Integer.valueOf(String.valueOf(propertyValue)) + " WHERE NAME = '" + itemName + "'";
		}		
		
		s.execute(updateStatement);
	}
	
	/**
	 * Delete an item in the database
	 * @param item specific item
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public static void deleteItem(Item item) throws SQLException, ClassNotFoundException {
		Statement s = getStatement(); // delete
		s.execute("DELETE FROM INVENTORY WHERE NAME ='" + item.getName() +"'");
	}
	
	/**
	 * Delete an array of items in the database
	 * 
	 * @param items
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public static void deleteItems(List<Item> items) throws SQLException, ClassNotFoundException {
		for (int index = 0; index < items.size(); index++) {
			deleteItem(items.get(index));
		}
	}
	
	/**
	 * Insert an item into the database
	 * @param item
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static void insertItem(Item item) throws SQLException, ClassNotFoundException {
		Statement s = getStatement();
		String queryStatement = "INSERT INTO INVENTORY VALUES (NULL, '" + item.getName() + "', " +
								item.getQuantity() + ", " + item.getManufacturingCost() + ", " +
								item.getSellPrice() + ", " + item.getReorderPoint() + ", " +
								item.getReorderAmount() + ", " + item.getTemperature() + ");";
		s.execute(queryStatement);
	}
	
	/**
	 * Insert a list of items into database
	 * 
	 * @param items
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void insertItems(List<Item> items) throws ClassNotFoundException, SQLException {
		for (int index = 0; index < items.size(); index++) {
			insertItem(items.get(index));
		}
	}
	
	/**
	 * Select all data from database
	 * 
	 * @return a hashmap which contains the whold data
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static HashMap<String, Item> selectAllItems() throws SQLException, ClassNotFoundException {
		HashMap<String, Item> resultMap = new HashMap<String, Item>();
		Statement s = getStatement();
		resultSet = s.executeQuery("SELECT * FROM INVENTORY;");
		
		if (resultSet != null) {
			while (resultSet.next()) {
				String  name = resultSet.getString("name");
				int quality  = resultSet.getInt("quantity");
				double manufacturing_cost = resultSet.getDouble("manufacturing_cost");
				double sell_price = resultSet.getDouble("sell_price");
				int reorder_point = resultSet.getInt("reorder_point");
				int reorder_amount = resultSet.getInt("reorder_amount");
				double temperature = resultSet.getDouble("temperature");
	        
				Item item = null;
				
				try { // this item is queried from database which means it is impossible to be invalid
					item = new Item(name, quality, manufacturing_cost, sell_price,
							reorder_point, reorder_amount, temperature);
				} catch (InappropriateValueException e) {}
	        
				resultMap.put(item.getName(), item);	  
			}
		}
		
		return resultMap;
	}
	
	/**
	 * Select the capital from database
	 * 
	 * @return capital amount
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static double selectCapital() {
		Statement s;
		double capital = 0;
		
		try {
			s = getStatement();
			resultSet = s.executeQuery("SELECT * FROM STORE;");
			if (resultSet != null) {
				while (resultSet.next()) {
					capital = resultSet.getDouble("money");
				}
			}
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return capital;
	}
	
	/**
	 * Update capital
	 * 
	 * @param capital
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void updateCaptial(double capital) throws ClassNotFoundException, SQLException {
		Statement s = getStatement();
		String updateStatement = "UPDATE STORE SET money = " + capital + " WHERE ID = " + 1;
		s.execute(updateStatement);
	}
	
	/**
	 * Close the connection to the database\
	 * 
	 * @throws SQLException
	 */
	public static void closeDBConnection() throws SQLException {
		if (resultSet != null) {
			resultSet.close();
		}
		if (statement != null) {
			statement.close();
		}		
		if (connection != null) {
			connection.close();
		}		
	}
	
	/**
	 * Commit the changes to database
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void commitChanges() throws ClassNotFoundException, SQLException {
		getConnection().commit();
	}
	
}
