package org.main.tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * File tools 
 * 
 * @author Junshao Lin
 *
 */
public class FileTools {
	
	/**
	 * Create a load steam of a file
	 * 
	 * @param filePath the path of the file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static BufferedReader loadFile(String filePath) throws FileNotFoundException {		
		FileReader fileReader = new FileReader(filePath);
		return new BufferedReader(fileReader);		
	}
	
}
