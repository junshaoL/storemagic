package org.main.model;

import java.sql.SQLException;
import java.util.ArrayList;

import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;

/**
 * Truck 
 * 
 * @author Junshao Lin
 *
 */
public abstract class Truck {
	
	protected ArrayList<Item> truckItems;
	protected Stock stock;
	
	public Truck() {
		truckItems = new ArrayList<>();
		stock = Stock.getInstace();
	}
	
	/**
	 * Load item to truck
	 * 
	 * @param item
	 */
	public void load(Item item) {
		truckItems.add(item);
	}
	
	/**
	 * Unload all items to a inventory
	 * 
	 * @throws DeliveryException
	 * @throws StockException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InappropriateValueException
	 */
	public void unload() throws DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		stock.updateItemsQuantity(truckItems, true);
	}
	
	/**
	 * Calculate the cost of this car
	 * @return
	 */
	public abstract double truckCost();
	
	
}
