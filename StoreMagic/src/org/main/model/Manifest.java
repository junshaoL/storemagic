package org.main.model;


import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.main.exceptions.CSVFormatException;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.tools.CSVFormatChecker;
import org.main.tools.Constants;
import org.main.tools.FileTools;
/**
 * create manifest
 * @author ZIQIAN ZHANG
 *
 */
public class Manifest {
	
	private HashMap<String, Item> reorderList = new HashMap<String, Item>();
	private int rfTruckCapacity = 800;
	private int odTruckCapacity = 1000;
	private double lowestTemp = 0;	
	private Truck truck;
	
	/**
	 * Load manifest file into stock
	 * 
	 * @param filePath manifest file path
	 * @throws IOException
	 * @throws CSVFormatException
	 * 
	 * @author Junshao Lin
	 * @throws StockException 
	 * @throws DeliveryException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InappropriateValueException 
	 */
	public void loadManifest(String filePath) throws IOException, CSVFormatException, DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		if (filePath == null || filePath.equals(""))
			return;
		
		BufferedReader buf = FileTools.loadFile(filePath);
		String itemData;
		
		CSVFormatChecker.resetBooleanForManifest();
		while ((itemData = buf.readLine()) != null) {
			if (!CSVFormatChecker.checkManifest(itemData))
				throw new CSVFormatException();
			checkTruck(itemData);
		}
		payFullBill(); // Pay the bill of the last truck
	}
	
	/**
	 * Check all the items in a truck
	 * If every item has been checked, processing payment action
	 * 
	 * @param itemData item data
	 * 
	 * @author Junshao Lin
	 * @throws StockException 
	 * @throws DeliveryException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InappropriateValueException 
	 */
	private void checkTruck(String itemData) throws DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		String[] splitData = itemData.split(",");
		if (splitData.length == 1) {
			if (truck != null) {
				payFullBill();
			}
			if (splitData[0].equals(Constants.REFRIGERATED_NAME)) 
				truck = new RefrigeratedTruck();
			else 
				truck = new OrdinaryTruck();
		}
		else {
			truck.load(new Item(splitData[0], Integer.parseInt(splitData[1])));
		}		
	}
	
	/**
	 * Pay the bill for income items
	 * 
	 * @author Junshao Lin
	 * @throws StockException 
	 * @throws DeliveryException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InappropriateValueException 
	 */
	private void payFullBill() throws DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		truck.unload();
		Store store = Store.getInstace();
		store.reduceCapital(truck.truckCost());
	}
	
	/**
	 * According to amount of item which store in database to compare with reorder point to create reorder list
	 * @param inventory inventory item data
	 * @return the item which need to be reorder
	 */
	public HashMap<String, Item> exportReorderList(HashMap<String, Item> inventory) {
		
		for (Map.Entry<String, Item> entry : inventory.entrySet()) {  
			if (entry.getValue().getQuantity() <= entry.getValue().getReorderPoint()) {
				reorderList.put(entry.getKey(), entry.getValue());
			} 
		}  
		return reorderList;
	}
	
	/**
	 * Rank the temperature of item
	 * @param reorderList Record the Item which need to reorder 
	 * @return Rank list according to item's temperature (low to high)
	 */
	public Item[] exportTempRank(HashMap<String, Item> reorderList) {
		Item[] tempRank = new Item[reorderList.size()];
		Boolean firstTemp = true;
		int i = 0;
		int rank = 0;
		for (int e = 0; e < tempRank.length - 1; e++) {
			Item defaultItem = new Item();
			defaultItem.setTemperature(Constants.TEMP_NO_NEED);
			tempRank[e] = defaultItem;
		}
		for (Map.Entry<String, Item> entry : reorderList.entrySet()) {  
			if (entry.getValue().getTemperature() != Constants.TEMP_NO_NEED) { //entry.getValue().getTemperature() != null
				if (firstTemp) { //first time to input temperature
					lowestTemp = entry.getValue().getTemperature();
					tempRank[i] = entry.getValue();//store it into rank 0
					firstTemp = false;
				}else if (entry.getValue().getTemperature() <= lowestTemp) {// if it low than lowestTemp
					lowestTemp = entry.getValue().getTemperature(); //it become lowest temperature
					for(int b = tempRank.length - 1; b > 0; b--) {
						tempRank[b] = tempRank[b - 1];
					}
					tempRank[0] = entry.getValue();//store it into rank 0
				}else {
					for (int c = 0; c < tempRank.length - 1; c++) {
						if (entry.getValue().getTemperature() > tempRank[c].getTemperature()) {
							rank = c + 1;
						}else {
							for(int b = tempRank.length - 1; b > rank; b--) {
								tempRank[b] = tempRank[b - 1];
							}
							tempRank[rank] = entry.getValue();
							break;
						}
					}
				}
				
			}
		}

		return tempRank;
	}
	
	/**
	 * create normal items array
	 * @param reorderList Record the reorder Item
	 * @return normal items array
	 */
	public Item[] exportOrdItem(HashMap<String, Item> reorderList) {
		Item[] ordItem = new Item[reorderList.size()];
		int i = 0;
		for (Map.Entry<String, Item> entry : reorderList.entrySet()) {
			if (entry.getValue().getTemperature() == Constants.TEMP_NO_NEED) {
				ordItem[i] = entry.getValue();
				i++;
			}
		}
		return ordItem;
	}
	/**
	 * 
	 * @param tempRank Temperature limited items array
	 * @param ordItem Normal items array
	 * @throws IOException
	 */
	public void exportManifest(Item[] tempRank, Item[] ordItem, String filepath) throws IOException {
		rfTruckCapacity = 800;
		odTruckCapacity = 1000;
		int currentOrderAmount = 0;
		boolean firstFT = true;
		boolean firstOT = true;
		FileWriter fw = new FileWriter(filepath,false); //target file path
		for (int i = 0; i < tempRank.length; i++) { //create rFManifest
			if (tempRank[i].getTemperature() != Constants.TEMP_NO_NEED) {
				currentOrderAmount = tempRank[i].getReorderAmount(); //record current item's reorder amount
				if (firstFT | rfTruckCapacity == 0) {  //first time to write or capacity is 0
					firstFT = false;
					fw.write(">Refrigerated\n");
					rfTruckCapacity = 800;//initial capacity of truck 
					while (rfTruckCapacity < currentOrderAmount) {  // if the amount of reorder items is more than truck capacity
						fw.write(tempRank[i].getName() + "," + rfTruckCapacity + "\n");  //full this truck
						currentOrderAmount -= rfTruckCapacity;//update current item's released reorder amount
						fw.write(">Refrigerated\n");  //add a new truck
						rfTruckCapacity = 800;  //initial capacity of truck 
						
					}
					fw.write(tempRank[i].getName() + "," + currentOrderAmount + "\n");
					rfTruckCapacity = rfTruckCapacity - currentOrderAmount;  //update capacity of truck
					
				}else {
					while (rfTruckCapacity < currentOrderAmount) {  // if the amount of reorder items is more than truck capacity
						fw.write(tempRank[i].getName() + "," + rfTruckCapacity + "\n");  //full this truck
						fw.write(">Refrigerated\n");  //add a new truck
						currentOrderAmount -= rfTruckCapacity;//update current item's released reorder amount
						rfTruckCapacity = 800;  //initial capacity of truck 
						
					}
					fw.write(tempRank[i].getName() + "," + currentOrderAmount + "\n");
					rfTruckCapacity = rfTruckCapacity - currentOrderAmount;  //update capacity of truck
				}
			}else {
				break;//there is no item in this array
			}
		}
		
		
		for (int i = 0; i < ordItem.length; i++) { //create oDManifest
			if (ordItem[i] != null) {
				currentOrderAmount = ordItem[i].getReorderAmount(); //record current item's reorder amount
				if (rfTruckCapacity != 0 && rfTruckCapacity < currentOrderAmount) { //if last rf truck is not full and not enough to store everything for this time
					fw.write(ordItem[i].getName() + "," + rfTruckCapacity + "\n");
					currentOrderAmount = currentOrderAmount - rfTruckCapacity; //get the released amount
					rfTruckCapacity = 0; //last rf truck is full
					fw.write(">Ordinary\n"); // start to write od truck list
					odTruckCapacity = 1000; //Initial capacity
					while (odTruckCapacity < currentOrderAmount) { //if one truck is not enough to store all thing
						fw.write(ordItem[i].getName() + "," + odTruckCapacity + "\n"); //as more as possible to store
						fw.write(">Ordinary\n"); //add new truck
						currentOrderAmount -= odTruckCapacity; //calculate the released amount of this item 
						odTruckCapacity = 1000; //Initial capacity
						
					}
					if (currentOrderAmount != 0) { //if there are released amount of item
						fw.write(ordItem[i].getName() + "," + currentOrderAmount + "\n");
						odTruckCapacity -= currentOrderAmount; //update capacity
					}
					firstOT = false;
				}
				if (rfTruckCapacity > currentOrderAmount) { //if last rf truck is enough to store this item
					fw.write(ordItem[i].getName() + "," + currentOrderAmount + "\n");
					rfTruckCapacity -= currentOrderAmount; //update released capacity of rf truck
					
				}
				if (rfTruckCapacity == 0) { // rf truck already full
					if (firstOT || odTruckCapacity == 0) {//if first time to write or od truck capacity is 0
						firstOT = false;
						fw.write(">Ordinary\n");
						odTruckCapacity = 1000;//Initial capacity
						
					}
					while (odTruckCapacity < currentOrderAmount) {
						fw.write(ordItem[i].getName() + "," + odTruckCapacity + "\n");
						fw.write(">Ordinary\n");
						currentOrderAmount -= odTruckCapacity; //calculate the released amount of this item 
						odTruckCapacity = 1000;//Initial capacity
						
					}
					if (currentOrderAmount != 0) { //if there are released amount of item
						fw.write(ordItem[i].getName() + "," + currentOrderAmount + "\n");
						odTruckCapacity -= currentOrderAmount; //update released capacity of rf truck
					}
				}
				
			}else {
				break; //there is no item in this array
			}
		}
		
		fw.close();
	}
	
	
}
