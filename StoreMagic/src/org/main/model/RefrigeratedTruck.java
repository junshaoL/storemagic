package org.main.model;

public class RefrigeratedTruck extends Truck{	

	@Override
	public double truckCost() {
		// TODO Auto-generated method stub
		double minTemp = 0.0;

		for (int index = 0; index < truckItems.size(); index++) {
			double itemTemp = stock.getItemTemp(truckItems.get(index).getName());
			minTemp = itemTemp < minTemp? itemTemp : minTemp;
		}
		
		return 900.0 + 200.0 * Math.pow(0.7, minTemp / 5.0);
	}

}
