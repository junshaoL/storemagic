package org.main.model;

public class OrdinaryTruck extends Truck {

	@Override
	public double truckCost() {
		// TODO Auto-generated method stub
		int totalQuantity = 0;
		for (int index = 0; index < truckItems.size(); index++) {
			totalQuantity += truckItems.get(index).getQuantity();
		}
		
		return 750 + 0.25 * totalQuantity;
	}

}
