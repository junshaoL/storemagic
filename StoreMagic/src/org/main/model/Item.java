package org.main.model;

import org.main.exceptions.InappropriateValueException;

/**
 * Item Class
 * 
 * @author Junshao Lin
 *
 */
public class Item {
	
	private String name;
	private int quantity;
	private double manufacturingCost;
	private double sellPrice;
	private int reorderPoint;
	private int reorderAmount;
	private double Temperature;
	
	public Item() {
	}
	
	public Item(String name, int quantity) {
		this.name = name;
		this.quantity = quantity;
	}
	
	public Item(String name, int quantity, double manufacturingCost, double sellPrice, int reorderPoint, int reorderAmount, double Temperature) throws InappropriateValueException {
		if (quantity < 0 | manufacturingCost < 0 | sellPrice < 0 |
				reorderPoint < 0 | reorderAmount < 0)
			throw new InappropriateValueException();
		this.name = name;
		this.quantity = quantity;
		this.manufacturingCost = manufacturingCost;
		this.sellPrice = sellPrice;
		this.reorderPoint = reorderPoint;
		this.reorderAmount = reorderAmount;
		this.Temperature = Temperature;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) throws InappropriateValueException {
		if (quantity < 0)
			throw new InappropriateValueException();
		this.quantity = quantity;
	}

	public double getManufacturingCost() {
		return manufacturingCost;
	}
	
	public void setManufacturingCost(double manufacturingCost) throws InappropriateValueException {
		if (manufacturingCost < 0)
			throw new InappropriateValueException();
		this.manufacturingCost = manufacturingCost;
	}
	
	public double getSellPrice() {
		return sellPrice;
	}
	
	public void setSellPrice(double sellPrice) throws InappropriateValueException {
		if (sellPrice < 0)
			throw new InappropriateValueException();
		this.sellPrice = sellPrice;
	}
	
	public int getReorderPoint() {
		return reorderPoint;
	}
	
	public void setReorderPoint(int reorderPoint) throws InappropriateValueException {
		if (reorderPoint < 0)
			throw new InappropriateValueException();
		this.reorderPoint = reorderPoint;
	}
	
	public int getReorderAmount() {
		return reorderAmount;
	}
	
	public void setReorderAmount(int reorderAmount) throws InappropriateValueException {
		if (reorderAmount < 0)
			throw new InappropriateValueException();
		this.reorderAmount = reorderAmount;
	}
	
	public double getTemperature() {
		return Temperature;
	}
	
	public void setTemperature(double temperature) {
		Temperature = temperature;
	}
		
}
