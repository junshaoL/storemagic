package org.main.model;

import java.sql.SQLException;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;

import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.tools.DBHelper;

/**
 * Stork 
 * Include inventory 
 * 
 * @author Junshao Lin
 *
 */
public class Stock {
	
	private static final Stock instance = new Stock();
	
	private HashMap<String, Item> inventory;
	private HashMap<String, Item> tempInventory;
	
	private Stock() {
		try {
			inventory = DBHelper.selectAllItems();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Stock getInstace() {
		return instance;
	}
	
	/**
	 * Get Inventory
	 * 
	 * @return hashMap of Inventory
	 */
	public HashMap<String, Item> getInventory() {
		return inventory;
	}
	
	/**
	 * Reset inventory data by selecting from the database
	 */
	public void refreshData() {
		try {
			inventory = DBHelper.selectAllItems();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Increase Item quantity
	 * @param item object include name and quantity
	 * 
	 * @author Junshao Lin
	 * @throws DeliveryException 
	 * @throws StockException 
	 * @throws InappropriateValueException 
	 */
	@SuppressWarnings("unchecked")
	public void increaseItemQuantity(Item item) throws DeliveryException, StockException, InappropriateValueException {
		if (tempInventory == null)
			tempInventory = (HashMap<String, Item>) inventory.clone();
		
		Item existItem = tempInventory.get(item.getName());
		double itemPrice;		
		
		
		if (existItem == null) { // should report an exception
			clearTempInventory();
			throw new DeliveryException();
		}
		else {
			existItem.setQuantity(existItem.getQuantity() + item.getQuantity());
		}
			
		itemPrice = existItem.getSellPrice();
			
		payForItems(item.getQuantity(), itemPrice);
		
	}
	
	/**
	 * Decrease Item quantity
	 * 
	 * @param addItem
	 * @throws StockException
	 * @throws InappropriateValueException
	 */
	@SuppressWarnings("unchecked")
	public void decreaseItemQuantity(Item item) throws StockException, InappropriateValueException {		
		if (tempInventory == null)
			tempInventory = (HashMap<String, Item>) inventory.clone();
		
		Item existItem = tempInventory.get(item.getName());
		double itemPrice;
		
		if (existItem == null) { // should report an exception
			clearTempInventory();
			throw new StockException();
		}
		else {
			int newQuantity = existItem.getQuantity() - item.getQuantity();
			if (newQuantity < 0)
				throw new StockException();
			existItem.setQuantity(newQuantity);
		}
		
		itemPrice = existItem.getSellPrice();
		
		revenueFromItems(item.getQuantity(), itemPrice);
	}
	
	/**
	 * Update quantities of  a list of items
	 * 
	 * @param container
	 * @param ifFromManifest
	 * @throws DeliveryException
	 * @throws StockException
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InappropriateValueException 
	 */
	public void updateItemsQuantity(List<Item> container, boolean ifIncrease) throws DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		// TODO Auto-generated method stub
		for (int index = 0; index < container.size(); index++) {
			if (ifIncrease) {
				increaseItemQuantity(container.get(index));
			}
			else {
				decreaseItemQuantity(container.get(index));
			}						
		}
		
		applyTempToRealInventory();
		Store.getInstace().applyTempCapitalToReal();
		updateDataQuantity();
	}
	
	/**
	 * Reduce capital
	 * @param item
	 * @throws StockException 
	 */
	private void payForItems(int quantity, double price) throws StockException {
		Store.getInstace().reduceCapital(quantity * price);
	}
	
	/**
	 * Increase amount
	 * 
	 * @param quantity
	 * @param price
	 */
	private void revenueFromItems(int quantity, double price) {
		Store.getInstace().increaseCapital(quantity * price);
	}
	
	/**
	 * Get item price based on the item name
	 * @param itemName
	 * @return price
	 */
	public double getItemPrice(String itemName) {
		return inventory.get(itemName).getSellPrice();
	}
	
	/**
	 * Get item temperature based on the item name
	 * @param itemName
	 * @return temperature
	 */
	public double getItemTemp(String itemName) {
		return inventory.get(itemName).getTemperature();
	}

	/**
	 * Get the amount all items (the amount of Quantities) 
	 * 
	 * @return the amount
	 */
	public int getItemAmount() {
		// TODO Auto-generated method stub
		int amount = 0;
		Iterator<Item> itemsIterator = inventory.values().iterator();
		
		while (itemsIterator.hasNext()) {
			amount += itemsIterator.next().getQuantity();
		}
		
		return amount;
	}
	
	/**
	 * Make sure apply changes to real inventory
	 */
	@SuppressWarnings("unchecked")
	public void applyTempToRealInventory() {
		inventory = (HashMap<String, Item>) tempInventory.clone();		
		clearTempInventory();
	}
	
	/**
	 * Update the database to teh new data 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateDataQuantity() throws ClassNotFoundException, SQLException {
		Iterator<Item> iterator = inventory.values().iterator();
		
		while (iterator.hasNext()) {
			Item item = iterator.next();
			DBHelper.updateInventory("quantity", item.getName(), item.getQuantity());
		}
		
		DBHelper.commitChanges();
	}
	
	/**
	 * Clear temp inventory
	 */
	public void clearTempInventory() {
		tempInventory.clear();
		tempInventory = null;
	}

}
