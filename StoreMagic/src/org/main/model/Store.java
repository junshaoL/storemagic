package org.main.model;

import java.sql.SQLException;

import org.main.exceptions.StockException;
import org.main.tools.DBHelper;

/**
 * Store Class
 * represent the capital of the store
 * functions which manipulate the capital
 * 
 *
 */
public class Store {
	
	private static final Store instance = new Store();	
	private double capital;
	private double tempCaptital;
	
	private Store() {
		capital = DBHelper.selectCapital();
	}
	
	public static Store getInstace() {
		return instance;
	}

	/**
	 * Get capital of the store
	 * @return the capital
	 * @author Junshao Lin
	 */
	public double getCapital() {
		return capital;
	}
	
	/**
	 * reduce the capital
	 * @param cost the cost of items
	 * @author Junshao Lin
	 * @throws StockException 
	 */
	public void reduceCapital(double cost) throws StockException {
		if (tempCaptital == 0)
			tempCaptital = capital;
		tempCaptital -= cost;
		if (tempCaptital < 0) {
			tempCaptital = 0;
			throw new StockException();			
		}
	}
	
	/**
	 * 
	 * @param revenue
	 * @author Junshao Lin
	 */
	public void increaseCapital(double revenue) {
		if (tempCaptital == 0)
			tempCaptital = capital;
		tempCaptital += revenue;
	}
	
	/**
	 * 
	 * @param capital
	 * @author Junshao Lin
	 */
	public void setCapital(double capital) {
		this.capital = capital;
	}
	
	/**
	 * Apply the temp capital to real capital
	 */
	public void applyTempCapitalToReal() {
		capital = tempCaptital;
		tempCaptital = 0;
	}
	
	/**
	 * Update capital to database
	 * 
	 * @author Junshao Lin
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateCapitalToDatabase() throws ClassNotFoundException, SQLException {
		DBHelper.updateCaptial(capital);
		DBHelper.commitChanges();
	}
	
}
