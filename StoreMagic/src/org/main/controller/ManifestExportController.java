package org.main.controller;

import java.io.IOException;
import java.util.HashMap;

import org.main.model.Item;
import org.main.model.Manifest;
import org.main.model.Stock;

public class ManifestExportController {
	
	public void exportManifest(String filePath) throws IOException {
		if (filePath == null | filePath == "")
			return;
		Manifest manifest = new Manifest();
		HashMap<String, Item> reorderList = manifest.exportReorderList(Stock.getInstace().getInventory());
		Item[] temperatureItem = manifest.exportTempRank(reorderList);
		Item[] ordinaryItem = manifest.exportOrdItem(Stock.getInstace().getInventory());
		manifest.exportManifest(temperatureItem, ordinaryItem, filePath);
	}
}
