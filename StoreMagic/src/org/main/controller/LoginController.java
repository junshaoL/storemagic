package org.main.controller;

import org.main.tools.Constants;

/**
 * Control the logic of Login UI
 * 
 * @author Junshao Lin
 *
 */
public class LoginController {
	
	/**
	 * Check the authority of a current user
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean authorityCheck(String username, String password) {
		if (username.equals(Constants.USERNAME) && password.equals(Constants.USERPASSWORD)) {
			return true;
		}
		return false;
	}
	
}
