package org.main.controller;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Iterator;

import org.main.GUI.HomeUI;
import org.main.model.Item;
import org.main.model.Stock;
import org.main.model.Store;
import org.main.tools.DBHelper;

/**
 * Controll the login of Home UI
 * 
 * @author Junshao Lin
 *
 */
public class HomeController implements ActionListener{

	private HomeUI home;
	
	public HomeController(HomeUI home) {
		this.home = home;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Button btn = (Button)e.getSource();
		if (btn.getName().equals("LManifest")) {
			home.goForLoadManifest();
		}
		else if (btn.getName().equals("LSalesLog")) {
			home.goForLoadSalesLog();
		}
		else if (btn.getName().equals("LItemProperty")) {
			home.goForLoadItemProperty();
		}
		else if (btn.getName().equals("GManifest")) {
			home.goForGenerateManifest();
		}
	}
	
	/**
	 * Check If the inventory needs replenishing
	 *  
	 * @return how many categories of items need replenishing
	 */
	public int checkIfNeedReplenishment() {
		
		Iterator<Item> iterator = Stock.getInstace().getInventory().values().iterator();
		int replenishItemNumber = 0;
		
		while (iterator.hasNext()) {
			Item item = iterator.next();
			if (item.getQuantity() <= item.getReorderPoint()) {
				replenishItemNumber++;
			}
		}
		
		return replenishItemNumber;
	}
	
	/**
	 * Get the capital of the store
	 * 
	 * @return capital
	 */
	public double getCapital() {
		return Store.getInstace().getCapital();
	}
	
	/**
	 * Get the inventory size
	 * 
	 * @return the size of the inventory
	 */
	public int getInventorySize() {
		return Stock.getInstace().getInventory().size();
	}
	
	/**
	 * Update Capital database
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateCapital() throws ClassNotFoundException, SQLException {
		Store.getInstace().updateCapitalToDatabase();
	}
	
	/**
	 * Do necessary operations when users click close the program
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void doWhenFinishProgram() throws ClassNotFoundException, SQLException {
		DBHelper.commitChanges();
		DBHelper.closeDBConnection();
	}
	
}
