package org.main.controller;

import java.io.IOException;
import java.sql.SQLException;

import org.main.exceptions.CSVFormatException;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.model.Manifest;

/**
 * Controller for Manifest
 * Manipulate the main logic of this part of function
 * 
 * @author Junshao Lin
 *
 */
public class ManifestLoadController {
	
	private Manifest manifest;
	
	public ManifestLoadController() {
		manifest = new Manifest();
	}
	
	/**
	 * Load manifest file into stock
	 * 
	 * @param filePath manifest file path
	 * @throws IOException
	 * @throws CSVFormatException
	 * 
	 * @author Junshao Lin
	 * @throws StockException 
	 * @throws DeliveryException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InappropriateValueException 
	 */
	public void loadManifest(String filePath) throws IOException, CSVFormatException, DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		manifest.loadManifest(filePath);
	}
	
}
