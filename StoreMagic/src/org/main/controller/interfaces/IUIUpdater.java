package org.main.controller.interfaces;

/**
 * UI updater interface
 * 
 * @author Junshao Lin
 *
 */
public interface IUIUpdater {
	
	/**
	 * Update the UI
	 */
	public void updateUI();
}
