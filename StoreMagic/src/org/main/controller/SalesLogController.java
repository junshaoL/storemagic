package org.main.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;

import org.main.exceptions.CSVFormatException;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.model.Item;
import org.main.model.Stock;
import org.main.model.Store;
import org.main.tools.CSVFormatChecker;
import org.main.tools.FileTools;
/**
 * Controller for sale log
 * @author zq
 *
 */
public class SalesLogController {
	/**
	 * update store stock according to imported sale log 
	 * @param filePath sale log file path
	 * @throws IOException
	 * @throws CSVFormatException
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws NumberFormatException 
	 * @throws DeliveryException 
	 * @throws StockException 
	 * @throws InappropriateValueException 
	 */
	public void loadSaleLog (String filePath) throws IOException, CSVFormatException, NumberFormatException, ClassNotFoundException, SQLException, StockException, InappropriateValueException {
		if (filePath == null || filePath.equals(""))
			return;
		
		BufferedReader log = FileTools.loadFile(filePath);
		Stock stock = Stock.getInstace();
		String logData;
		
		while ((logData = log.readLine()) != null) {
			if (!CSVFormatChecker.checkSalesLog(logData)) {
				throw new CSVFormatException();
			} else {
				String[] values = logData.split(",");
				Item updateItem = new Item();
				updateItem.setName(values[0]);
				updateItem.setQuantity(Integer.parseInt(values[1]));
				stock.decreaseItemQuantity(updateItem);
			}
		}
		
		stock.applyTempToRealInventory();	
		Store.getInstace().applyTempCapitalToReal();
		stock.updateDataQuantity();
	}
	
}
