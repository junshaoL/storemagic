package org.main.controller;

import java.util.HashMap;
import java.util.Iterator;

import org.main.model.Item;
import org.main.model.Stock;
import org.main.tools.Constants;
import org.main.tools.DecimalTools;

/**
 * Control the logic of Inventory UI
 * 
 * @author Junshao Lin
 *
 */
public class InventoryController {
	
	/**
	 * Get the item properties' name for the columns' name of JTable 
	 * 
	 * @return
	 */
	public String[] getItemPropertyName() {
		String[] itemPropertyName = {"Name", "Quantity", "Manufacturing Cost",
				"Sell Price", "Reorder Point", "Reorder Amount", "Temperature"};
		return itemPropertyName;
	}
	
	/**
	 * Get the item data of each property for the  JTable 
	 * 
	 * @return
	 */
	public String[][] getItemRealData() {
		String[][] data = null;
		
		Stock stock = Stock.getInstace();
		HashMap<String, Item> inventory = stock.getInventory();
		
		Iterator<Item> iterator = inventory.values().iterator();
		data = new String[inventory.size()][getItemPropertyName().length];
		int index = 0;
		
		while (iterator.hasNext()) {
			Item item = iterator.next();
			String[] currentLine = data[index];
			setDataToStringArray(item, currentLine);
			index++;
		}

		return data;
	}
	
	/**
	 * Set the data from an item into a String array
	 * 
	 * @param item
	 * @param dataContainer
	 */
	private void setDataToStringArray(Item item, String[] dataContainer) {
		dataContainer[0] = item.getName();
		dataContainer[1] = item.getQuantity() + "";
		dataContainer[2] = DecimalTools.formDouble(item.getManufacturingCost());
		dataContainer[3] = DecimalTools.formDouble(item.getSellPrice());
		dataContainer[4] = item.getReorderPoint() + "";
		dataContainer[5] = item.getReorderAmount() + "";
		if (item.getTemperature() == Constants.TEMP_NO_NEED) {
			dataContainer[6] = "";
		}
		else {
			dataContainer[6] = item.getTemperature() + "";
		}
	}
	
}
