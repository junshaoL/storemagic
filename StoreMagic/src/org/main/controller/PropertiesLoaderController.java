package org.main.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;

import org.main.exceptions.CSVFormatException;
import org.main.exceptions.InappropriateValueException;
import org.main.model.Item;
import org.main.model.Stock;
import org.main.tools.CSVFormatChecker;
import org.main.tools.Constants;
import org.main.tools.DBHelper;
import org.main.tools.FileTools;
/**+
 * when store just open, controller need to import the properties of item for setting up inventory
 * @author zq
 *
 */
public class PropertiesLoaderController {
	/**
	 * import file to set up database
	 * @param filePath The path of properties of items
	 * @throws IOException 
	 * @throws CSVFormatException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InappropriateValueException 
	 * @throws NumberFormatException 
	 * @author ZIQIAN ZHANG
	 */
	public void setProperties (String filePath) throws IOException, CSVFormatException, SQLException, ClassNotFoundException, NumberFormatException, InappropriateValueException {
		if (filePath == null | filePath == "")
			return;
		BufferedReader log = FileTools.loadFile(filePath);
		String logData;
		
		while ((logData = log.readLine()) != null) {
			if (!CSVFormatChecker.checkProperties(logData)) {
				throw new CSVFormatException();
			}else {
				Item item = new Item();
				String[] values = logData.split(",");
				if (values.length == 5) {
					if (Stock.getInstace().getInventory().containsKey(values[0])) {						
						DBHelper.updateInventory("Manufacturing_Cost", values[0], Double.parseDouble(values[1]));
						DBHelper.updateInventory("Sell_Price", values[0], Double.parseDouble(values[2]));
						DBHelper.updateInventory("Reorder_Point", values[0], Integer.parseInt(values[3]));
						DBHelper.updateInventory("Reorder_Amount", values[0], Integer.parseInt(values[4]));
						DBHelper.updateInventory("Temperature", values[0], Constants.TEMP_NO_NEED);
					}else {
						item.setName(values[0]);
						item.setQuantity(0);
						item.setManufacturingCost(Double.parseDouble(values[1]));
						item.setSellPrice(Double.parseDouble(values[2]));
						item.setReorderPoint(Integer.parseInt(values[3]));
						item.setReorderAmount(Integer.parseInt(values[4]));
						item.setTemperature(Constants.TEMP_NO_NEED);
						DBHelper.insertItem(item);
					}
				}else {
					if (Stock.getInstace().getInventory().containsKey(values[0])) {
						DBHelper.updateInventory("Manufacturing_Cost", values[0], Double.parseDouble(values[1]));
						DBHelper.updateInventory("Sell_Price", values[0], Double.parseDouble(values[2]));
						DBHelper.updateInventory("Reorder_Point", values[0], Integer.parseInt(values[3]));
						DBHelper.updateInventory("Reorder_Amount", values[0], Integer.parseInt(values[4]));
						DBHelper.updateInventory("Temperature", values[0], values[5]);
					}else {
						item.setName(values[0]);
						item.setQuantity(0);
						item.setManufacturingCost(Double.parseDouble(values[1]));
						item.setSellPrice(Double.parseDouble(values[2]));
						item.setReorderPoint(Integer.parseInt(values[3]));
						item.setReorderAmount(Integer.parseInt(values[4]));
						item.setTemperature(Integer.parseInt(values[5]));
						DBHelper.insertItem(item);
					}
				}
				
			}
		}
		
		DBHelper.commitChanges();
		Stock.getInstace().refreshData();
	}
}
