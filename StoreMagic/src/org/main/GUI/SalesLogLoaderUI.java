package org.main.GUI;

import java.io.IOException;
import java.sql.SQLException;

import org.main.controller.SalesLogController;
import org.main.controller.interfaces.IUIUpdater;
import org.main.exceptions.CSVFormatException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;

/**
 * Sales Log Loader UI
 * @author Junshao Lin
 *
 */
public class SalesLogLoaderUI {
	
	private SalesLogController salesLogController;
	private IUIUpdater updater;
	
	public SalesLogLoaderUI(IUIUpdater updater) throws IOException, CSVFormatException, StockException, InappropriateValueException {
		salesLogController = new SalesLogController();
		this.updater = updater;
		loadSalesLog();
	}
	
	private void loadSalesLog() throws IOException, CSVFormatException, StockException, InappropriateValueException {
		String filePath = FileOperator.loadFileUI("Chose your Sales Log");
		
		try {
			salesLogController.loadSaleLog(filePath);
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		updater.updateUI();
	}
		
}
