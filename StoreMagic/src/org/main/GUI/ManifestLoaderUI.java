package org.main.GUI;

import java.io.IOException;
import java.sql.SQLException;

import org.main.controller.ManifestLoadController;
import org.main.controller.interfaces.IUIUpdater;
import org.main.exceptions.CSVFormatException;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;

/**
 * Load manifest UI
 * @author Junshao Lin
 *
 */
public class ManifestLoaderUI {
	
	private ManifestLoadController manifestController;
	private IUIUpdater updater;
	
	public ManifestLoaderUI(IUIUpdater updater) throws IOException, CSVFormatException, DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		manifestController = new ManifestLoadController();
		this.updater = updater;
		loadManifest();
	}
	
	private void loadManifest() throws IOException, CSVFormatException, DeliveryException, StockException, ClassNotFoundException, SQLException, InappropriateValueException {
		String filePath = FileOperator.loadFileUI("Chose your manifest");
		manifestController.loadManifest(filePath);
		updater.updateUI();
	}
		
}
