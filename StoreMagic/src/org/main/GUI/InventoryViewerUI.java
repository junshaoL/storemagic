package org.main.GUI;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.main.controller.InventoryController;

/**
 * Inventory Viewer
 * Display all the information of the items in the inventory
 * 
 * @author Junshao Lin
 */
public class InventoryViewerUI extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private InventoryController inventoryController;
	
	public InventoryViewerUI() {
		inventoryController = new InventoryController();
		JTable inventoryTable = initializeJTable(inventoryController.getItemPropertyName(), inventoryController.getItemRealData());
		JScrollPane scrollPane = new JScrollPane(inventoryTable);
		this.add(scrollPane);
	}
	
	/**
	 * initialize JTable used to represent inventory data
	 * 
	 * @param itemPropertyName Property Name 
	 * @param inventoryData inventory data for a row
	 * @return
	 */
	private JTable initializeJTable(String[] itemPropertyName, String[][] inventoryData) {
		JTable inventoryTable = new JTable(inventoryData, itemPropertyName);
		
		inventoryTable.setPreferredScrollableViewportSize(new Dimension(850, 300));
		inventoryTable.setFillsViewportHeight(true);
		
		return inventoryTable;
	}
	
}
