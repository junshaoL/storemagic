package org.main.GUI;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
/**File Loader
 * Let user choice target file and get its file path
 * @author zq
 *
 */
public class FileOperator {
	/**
	 * let users choice target file and return its file path
	 * @return return the file path of .csv file which choose by user 
	 */
	public static String loadFileUI(String title) {
		JButton open = new JButton();
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new java.io.File("./"));
		fc.setDialogTitle(title);
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		if (fc.showOpenDialog(open) == JFileChooser.APPROVE_OPTION) {
			
		}
		
		if (fc.getSelectedFile() != null)
			return fc.getSelectedFile().getAbsolutePath();

		return null;
	}
	
	/**
	 * let users choice the file location and return its file path
	 * there will be a default file name which users can change it
	 * 
	 * @return return the file path of .csv file which user want to create 
	 */
	public static String createFileUI(String title) {
		String defaultFileName = "Manifest.csv";
		JButton open = new JButton();
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new java.io.File("./"));
		fc.setDialogTitle(title);
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.setSelectedFile(new File(defaultFileName));
		if (fc.showOpenDialog(open) == JFileChooser.APPROVE_OPTION) {
			
		}
		
		if (fc.getSelectedFile() != null)
			return fc.getSelectedFile().getAbsolutePath();

		return null;
	}
	
	
	
}
