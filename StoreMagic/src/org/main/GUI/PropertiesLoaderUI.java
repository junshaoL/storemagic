package org.main.GUI;

import java.io.IOException;
import java.sql.SQLException;

import org.main.controller.PropertiesLoaderController;
import org.main.controller.interfaces.IUIUpdater;
import org.main.exceptions.CSVFormatException;
import org.main.exceptions.InappropriateValueException;
/**
 * properties load UI
 * @author ZIQIAN ZHANG
 *
 */
public class PropertiesLoaderUI {
	private PropertiesLoaderController importProperties;
	private IUIUpdater updater;
	
	public PropertiesLoaderUI(IUIUpdater updater) throws IOException, CSVFormatException, ClassNotFoundException, SQLException, NumberFormatException, InappropriateValueException {
		importProperties = new PropertiesLoaderController();
		this.updater = updater;
		loadProperties();
	}
	
	private void loadProperties() throws IOException, CSVFormatException, ClassNotFoundException, SQLException, NumberFormatException, InappropriateValueException {
		String filePath = FileOperator.loadFileUI("Choice your item properties");
		importProperties.setProperties(filePath);
		updater.updateUI();
	}
}
