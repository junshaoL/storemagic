package org.main.GUI;

import java.io.IOException;

import org.main.controller.ManifestExportController;

/**
 * Open Generate manifest UI
 * 
 * @author Junshao Lin
 *
 */
public class ManifestGenerateUI {
	
	public ManifestGenerateUI() throws IOException {
		String filePath = FileOperator.createFileUI("Chose your target file path");
		ManifestExportController manifestExportController = new ManifestExportController();
		manifestExportController.exportManifest(filePath);
	}
	
}
