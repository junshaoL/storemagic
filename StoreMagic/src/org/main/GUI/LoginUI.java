package org.main.GUI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.main.controller.LoginController;

/**
 * Login UI
 * @author Junshao Lin
 *
 */
public class LoginUI extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LoginController loginController;
	
	private JTextField usernameText;
	private JPasswordField userpasswordText;
	private JButton jBtn;
	private boolean ifCheck = true;
	private KeyListener enterListener = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {}

		@Override
		public void keyPressed(KeyEvent e) {}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				if (ifCheck) {
					goToNextUI(loginController.authorityCheck(usernameText.getText(), 
							String.valueOf(userpasswordText.getPassword())));					
				}
				ifCheck = !ifCheck;
			}			
		}
		
	};
	
	/**
	 * Start login UI
	 */
	public LoginUI() {
		super("Login with Admin");
		loginController = new LoginController();
		drawLoginInterface();
		addListener();
	}
	
	/**
	 * Draw login UI
	 */
	private void drawLoginInterface() {
		
		GridLayout gLayout = new GridLayout(3, 2, 5, 5);
		JPanel jPanel = new JPanel();
		jPanel.setLayout(gLayout);
		
		JLabel labelUsername = new JLabel("UserName: ");
		JLabel labelPassword = new JLabel("Password: ");
		usernameText = new JTextField();
		userpasswordText = new JPasswordField();
		JLabel spaceHolder = new JLabel();
		jBtn = new JButton("Login");
		
		labelUsername.setHorizontalAlignment(JLabel.CENTER);
		labelPassword.setHorizontalAlignment(JLabel.CENTER);
		
		jPanel.add(labelUsername);
		jPanel.add(usernameText);
		jPanel.add(labelPassword);
		jPanel.add(userpasswordText);
		jPanel.add(spaceHolder);
		jPanel.add(jBtn);		
		
		this.add(jPanel);
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(400, 200);
		this.setVisible(true);
		this.setResizable(false);
	}
	
	/**
	 * Add listener to widgets 
	 */
	private void addListener() {
		jBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub					
				goToNextUI(loginController.authorityCheck(usernameText.getText(), 
						String.valueOf(userpasswordText.getPassword())));
			}
			
		});
		
		usernameText.addKeyListener(enterListener);
		userpasswordText.addKeyListener(enterListener);
	}
	
	/**
	 * Go to next UI if user has the authority
	 * 
	 * @param ifGo if user can go
	 */
	private void goToNextUI(boolean ifGo) {
		if (ifGo)
			goToHome();
		else {
			showErrorDialog();
		}	
	}
	
	/**
	 * Show error dialog
	 */
	private void showErrorDialog() {
		JOptionPane.showMessageDialog(this,
			    "User name or password is incorrect",
			    "Login error",
			    JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Go to home UI
	 */
	public void goToHome() {
		this.setVisible(false);		
		new HomeUI();
		this.dispose();
	}
	
	/**
	 * Application starts
	 */
	public static void main(String[] args) {
		new LoginUI();
	}
	
}
