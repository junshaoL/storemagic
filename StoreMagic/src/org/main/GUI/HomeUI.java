package org.main.GUI;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.main.controller.HomeController;
import org.main.controller.interfaces.IUIUpdater;
import org.main.exceptions.CSVFormatException;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.tools.Constants;
import org.main.tools.DecimalTools;

/**
 * The UI of Home
 *
 */
public class HomeUI extends JFrame implements IUIUpdater{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private HomeController homeController;
	private JPanel bottomPanel;
	private JLabel capitalAmountLabel;
	private JLabel messageLabel;
	private Button btnLoadManifest;
	private Button btnLoadSalesLog;
	private Button btnLoadItemProperty;
	private Button btnGenerateManifest;
	
	public HomeUI() {
		homeController = new HomeController(this);
		initializeHomeGUI();
	}
	
	/**
	 * Initialize Home GUI
	 * 
	 * @author Junshao Lin
	 */
	private void initializeHomeGUI() {
		this.setLayout(new BorderLayout());
		JPanel jTop = new JPanel(new GridLayout(1, 2));
		bottomPanel = new JPanel();
		JPanel jBottomLeft = new JPanel();
		JPanel buttons = new JPanel();
		
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		
		initializeBtn();
		
		capitalAmountLabel = new JLabel();
		messageLabel = new JLabel();
		
		setBtnName();
		
		capitalAmountLabel.setHorizontalAlignment(JLabel.RIGHT);
		messageLabel.setHorizontalAlignment(JLabel.LEFT);
		capitalAmountLabel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 10));	
		messageLabel.setBorder(BorderFactory.createEmptyBorder(20, 10, 0, 0));
		

		buttons.add(btnLoadManifest);
		buttons.add(btnLoadSalesLog);
		buttons.add(btnLoadItemProperty);
		buttons.add(btnGenerateManifest);
		
		jBottomLeft.add(buttons);

		bottomPanel.add(jBottomLeft);
		jTop.add(messageLabel);
		jTop.add(capitalAmountLabel);		
		updateUI();
		
		addBtnListener();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.add(jTop);
		this.add(bottomPanel, BorderLayout.SOUTH);
		this.setTitle("StoreMagic");
		this.pack();
		this.setVisible(true);
		this.setResizable(false);
		
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				try {
					homeController.updateCapital();
					homeController.doWhenFinishProgram();
				} catch (ClassNotFoundException | SQLException e1) {
					e1.printStackTrace();
				}
				super.windowClosing(e);
			}
			
		});
	}
	
	/**
	 * Update Inventory View UI
	 * 
	 * @author Junshao Lin
	 */
	private void updateInventoryUI() {
		if (bottomPanel.getComponentCount() > 1)
			bottomPanel.remove(1);
		InventoryViewerUI inventoryUI = new InventoryViewerUI();
		bottomPanel.add(inventoryUI);
		bottomPanel.revalidate();
		bottomPanel.repaint();
	}
	
	/**
	 * Initialize buttons
	 * 
	 * @author Junshao Lin
	 */
	private void initializeBtn() {
		btnLoadManifest = new Button("Load Manifest");
		btnLoadSalesLog = new Button("Load SalesLog");
		btnLoadItemProperty = new Button("Load Item Property");
		btnGenerateManifest = new Button("Generate Manifest");
	}
	
	/**
	 * Set buttons' name
	 * 
	 * @author Junshao Lin
	 */
	private void setBtnName() {
		btnLoadManifest.setName("LManifest");
		btnLoadSalesLog.setName("LSalesLog");
		btnLoadItemProperty.setName("LItemProperty");
		btnGenerateManifest.setName("GManifest");		
	}
	
	/**
	 * Add action listener to buttons
	 * 
	 * @author Junshao Lin
	 */
	private void addBtnListener() {
		btnLoadManifest.addActionListener(homeController);
		btnLoadSalesLog.addActionListener(homeController);
		btnLoadItemProperty.addActionListener(homeController);
		btnGenerateManifest.addActionListener(homeController);
	}	

	/**
	 * Set Capital message
	 * 
	 * @author Junshao Lin
	 */
	private void setCapitalMessage() {
		capitalAmountLabel.setText( "Capital: " + DecimalTools.formDouble(homeController.getCapital()));
	}
	
	/**
	 * Set warning message
	 * 
	 * @author Junshao Lin
	 */
	private void setIfNeedReplenishmentMessage() {
		String messageString = Constants.WELCOME_MESSAGE;
		int replenishItemNumber = homeController.checkIfNeedReplenishment();
		if (replenishItemNumber > 0) {
			messageString += replenishItemNumber + Constants.REPLENISHMENT_MESSAGE;
			btnGenerateManifest.setEnabled(true);
		}
		else {
			btnGenerateManifest.setEnabled(false);
		}
		
		messageLabel.setText(messageString);
	}
	
	/**
	 * Check if Buttons' state need to be changed
	 * 
	 * @return the state of buttons
	 * 
	 * @author Junshao Lin
	 */
	private boolean ifSwitchButtonState() {
		if (homeController.getInventorySize() > 0) 
			return true;
		return false;
	}
	
	/**
	 * Change the state of Buttons
	 * @param switchState
	 * 
	 * @author Junshao Lin
	 */
	private void switchButtonState(boolean switchState) {
		btnLoadManifest.setEnabled(switchState);
		btnLoadSalesLog.setEnabled(switchState);		
	}
	
	/**
	 * Open Load Manifest UI
	 * 
	 * @author Junshao Lin
	 */
	public void goForLoadManifest() {
		try {
			new ManifestLoaderUI(this);
		} catch (IOException | CSVFormatException | ClassNotFoundException | SQLException  e) {
			JOptionPane.showMessageDialog(this,
				    "Please choose a correct file with valid format",
				    "File error",
				    JOptionPane.ERROR_MESSAGE);
		} catch (InappropriateValueException e) {
			showInvalidValueDialog();
		} catch (DeliveryException e) {
			JOptionPane.showMessageDialog(this,
				    "There are items which the inventory does not include",
				    "Items error",
				    JOptionPane.ERROR_MESSAGE);
		} catch (StockException e) {
			JOptionPane.showMessageDialog(this,
				    "The capital can not afford this manifest",
				    "File error",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Open Load Sales Log UI
	 * @author zq
	 */
	public void goForLoadSalesLog() {
		try {
			new SalesLogLoaderUI(this);
		} catch (IOException | CSVFormatException e) {
			JOptionPane.showMessageDialog(this,
				    "Please choice a correct type file",
				    "Type error",
				    JOptionPane.ERROR_MESSAGE);
		} catch (StockException e) {
			JOptionPane.showMessageDialog(this,
				    "There are items which the inventory does not include",
				    "Items error",
				    JOptionPane.ERROR_MESSAGE);
		} catch (InappropriateValueException e) {
			showInvalidValueDialog();
		}
	}

	/**
	 * Open Load Item Property UI
	 * @author zq
	 */
	public void goForLoadItemProperty() {
		try {
			new PropertiesLoaderUI(this);
		} catch (IOException | CSVFormatException | ClassNotFoundException | SQLException  e) {
			//e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Please choice a correct type file",
					"File error", 
					JOptionPane.ERROR_MESSAGE);
		} catch (NumberFormatException | InappropriateValueException e) {
			showInvalidValueDialog();
		}
	}

	/**
	 * Open Generate Manifest UI
	 * @author zq
	 */
	public void goForGenerateManifest() {
		try {
			new ManifestGenerateUI();
		}catch(IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,
					"Please choice a correct file path",
					"File path error", 
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Show the dialog to present invalid value inputed
	 * 
	 * @author Junshao Lin
	 */
	private void showInvalidValueDialog() {
		JOptionPane.showMessageDialog(this,
				"There is an invaild value in the file.\nPlease check it before Input it",
				"Content error", 
				JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public synchronized void updateUI() {
		// TODO Auto-generated method stub
		setCapitalMessage();
		setIfNeedReplenishmentMessage();
		switchButtonState(ifSwitchButtonState());
		updateInventoryUI();
	}
	

}
