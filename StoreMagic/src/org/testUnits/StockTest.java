package org.testUnits;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.model.Item;
import org.main.model.Stock;

public class StockTest {

	private Stock stock = Stock.getInstace();
	
	@Test
	public void testIncreaseItemQuantity() {
		Item testItem = new Item();
		Item testItem2 = stock.getInventory().values().iterator().next();
		int testItemsCurrentQuantity = testItem2.getQuantity();
		try {
			stock.increaseItemQuantity(null);
			fail("a null object has been accpet");
		} catch (Exception e) {}
		
		
		testItem.setName("Hello");		
		try {
			stock.increaseItemQuantity(testItem);
			fail("accpet a wrong item");
		} catch (DeliveryException | StockException | InappropriateValueException e) {}
		
		try {
			Item testItem3 = new Item(testItem2.getName(), 12);
			stock.increaseItemQuantity(testItem3);
			stock.applyTempToRealInventory();
			if (stock.getInventory().get(testItem2.getName()).getQuantity() - testItemsCurrentQuantity != 12) {
				fail("result does not match");
			}
		} catch (InappropriateValueException | DeliveryException | StockException e) {
			fail("exception happens");
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDecreaseItemQuantity() {
		Item testItem = new Item();
		Item testItem2 = stock.getInventory().values().iterator().next();
		int testItemsCurrentQuantity = testItem2.getQuantity();
		try {
			stock.increaseItemQuantity(null);
			fail("a null object has been accpet");
		} catch (Exception e) {}
		
		
		testItem.setName("Hello");		
		try {
			stock.increaseItemQuantity(testItem);
			fail("accpet a wrong item");
		} catch (DeliveryException | StockException | InappropriateValueException e) {}
		
		try {
			Item testItem3 = new Item(testItem2.getName(), 1);
			stock.decreaseItemQuantity(testItem3);
			stock.applyTempToRealInventory();
			if (testItemsCurrentQuantity - stock.getInventory().get(testItem2.getName()).getQuantity() != 1) {
				fail("result does not match");
			}
		} catch (InappropriateValueException | StockException e) {
			fail("exception happens");
			e.printStackTrace();
		}
	}
	
	@Test
	public void testUpdateItemsQuantity() {
		ArrayList<Item> dataContainer = new ArrayList<Item>();
		dataContainer.add(new Item());
		dataContainer.add(new Item("hello", 12));
		
		try {
			stock.updateItemsQuantity(dataContainer, true);
			fail("accpect invalid input");
			stock.updateItemsQuantity(dataContainer, false);
			fail("accpect invalid input");
		} catch (ClassNotFoundException | DeliveryException | StockException | SQLException
				| InappropriateValueException e) {}
		
		dataContainer.clear();
		Iterator<Item> iterator = stock.getInventory().values().iterator();
		int index = 0;
		
		ArrayList<Integer> oldValue = new ArrayList<Integer>();
		
		for (; index < 3; index++) {
			if (iterator.hasNext()) {
				Item existItem = iterator.next();
				Item item = null;
				try {
					item = new Item(existItem.getName(), existItem.getQuantity(), existItem.getManufacturingCost(),
							existItem.getSellPrice(), existItem.getReorderPoint(), existItem.getReorderAmount(), existItem.getTemperature());
				} catch (InappropriateValueException e1) {
					e1.printStackTrace();
				}
				oldValue.add(existItem.getQuantity());
				try {
					item.setQuantity(1);
				} catch (InappropriateValueException e) {
					e.printStackTrace();
				}
				dataContainer.add(item);
			}
		}
		
		try {
			stock.updateItemsQuantity(dataContainer, true);
			stock.refreshData();
			
			for (int address = 0; address < dataContainer.size(); address++) {
				if (oldValue.get(address) - stock.getInventory().get(dataContainer.get(address).getName()).getQuantity() != -1)
					fail("result does not match");
			}
			
			stock.updateItemsQuantity(dataContainer, false);
			stock.refreshData();
			
			for (int address = 0; address < dataContainer.size(); address++) {
				if (oldValue.get(address) - stock.getInventory().get(dataContainer.get(address).getName()).getQuantity() != 0)
					fail("result does not match");
			}
			
		} catch (ClassNotFoundException | DeliveryException | StockException | SQLException
				| InappropriateValueException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testGetItemPrice() {
		Item testItem = stock.getInventory().values().iterator().next();
		double price = stock.getItemPrice(testItem.getName());
		assertEquals(testItem.getSellPrice(), price);
	}
	
	@Test
	public void testGetItemTemp() {
		Item testItem = stock.getInventory().values().iterator().next();
		double temp = stock.getItemTemp(testItem.getName());
		assertEquals(testItem.getTemperature(), temp);
	}
	
	@Test
	public void testGetItemAmount() {
		int amount = 0;
		Iterator<Item> itemsIterator = stock.getInventory().values().iterator();
		
		while (itemsIterator.hasNext()) {
			amount += itemsIterator.next().getQuantity();
		}
		
		assertEquals(stock.getItemAmount(), amount);
	}

}
