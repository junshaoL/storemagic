package org.testUnits;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.main.exceptions.StockException;
import org.main.model.Store;


public class StoreTest {		

	@Test
	public void intialStoreCaptialtest() {
		Store.getInstace().setCapital(100000);
		if (Store.getInstace().getCapital() != 100000) {
			fail("Initial captial error");
			
		}
	}
	
	@Test
	public void reduceAndIncreaseCaptialTest() {
		Store.getInstace().setCapital(100000);
		try {
			Store.getInstace().reduceCapital(30000);
			Store.getInstace().increaseCapital(10000);
			Store.getInstace().applyTempCapitalToReal();
		}catch (StockException e) {
			e.printStackTrace();
		} 
		if (Store.getInstace().getCapital() != 80000) {
			fail("Reduce captial error");
		}
	} 
	

}
