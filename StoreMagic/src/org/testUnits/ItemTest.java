package org.testUnits;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.main.exceptions.InappropriateValueException;
import org.main.model.Item;

public class ItemTest {
	
	/**
	 * Test the function about setting and getting the properties of a item 
	 * @author ZIQIAN ZHANG
	 * @throws InappropriateValueException 
	 */
	@Test
	public void testItem() {
		Item item1 = new Item();
		
		try {
			item1.setName("item1");
			item1.setQuantity(10);
			
			item1.setManufacturingCost(12.1);
			item1.setSellPrice(11.1);
			
			item1.setReorderAmount(300);
			item1.setReorderPoint(200);
			
			item1.setTemperature(-100);
		} 
		catch(InappropriateValueException e){
			System.out.println("Item datas are not set correctly");
		}
				
		assertEquals(item1.getName(), "item1");
		assertEquals(item1.getQuantity(), 10);
		assertEquals(item1.getSellPrice(), 11.1, 0.00);
		assertEquals(item1.getManufacturingCost(), 12.1, 0.00);
		assertEquals(item1.getReorderAmount(), 300);
		assertEquals(item1.getReorderPoint(), 200);
		assertEquals(item1.getTemperature(), -100, 0.00);
		
		Item item2 = new Item();
		
		try {
			item2.setManufacturingCost(-2);
			fail("Manufacturing cost should not be negative");
		} catch (InappropriateValueException e) {}
		
		try {
			item2.setQuantity(-1);
			fail("Quantity should not be negative");
		} catch (InappropriateValueException e) {}
		
		try {
			item2.setReorderAmount(-2);
			fail("Reorder Amount should not be negative");
		} catch (InappropriateValueException e) {}
		
		try {
			item2.setReorderPoint(-2);
			fail("Reorder Point should not be negative");
		} catch (InappropriateValueException e) {}
		
		try {
			item2.setSellPrice(-2);
			fail("Sell Price should not be negative");
		} catch (InappropriateValueException e) {}		
		
	}

}
