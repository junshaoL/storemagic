package org.testUnits;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import org.junit.Test;
import org.main.exceptions.CSVFormatException;
import org.main.exceptions.DeliveryException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.model.Item;
import org.main.model.Manifest;
import org.main.model.OrdinaryTruck;
import org.main.model.Stock;
import org.main.model.Store;
import org.main.tools.Constants;
import org.main.tools.FileTools;

public class ManifestTest {

	private HashMap<String, Item> testData = new HashMap<String, Item>(); 
	private Manifest manifest = new Manifest();;
	
	@Test
	public void testExportReorderList() {
		testData.clear();
		
		Item item1 = new Item();
		Item item2 = new Item();
		Item item3 = new Item();
		
		item1.setName("item1");
		item2.setName("item2");
		item3.setName("item3");
		
		try {
			item1.setQuantity(10);
			item2.setQuantity(2);
			item3.setQuantity(15);
			
			item1.setReorderPoint(10);
			item2.setReorderPoint(1);;
			item3.setReorderPoint(16);
			
		} catch (InappropriateValueException e) {}
		
		testData.put(item1.getName(), item1);
		testData.put(item2.getName(), item2);
		testData.put(item3.getName(), item3);		
		
		HashMap<String, Item> result = manifest.exportReorderList(testData);
		
		if (result.size() != 2)
			fail("result does not match");
		
		if (!result.keySet().contains("item1"))
			fail("result does not match");
		
		if (result.keySet().contains("item2"))
			fail("result does not match");
		
		if (!result.keySet().contains("item3"))
			fail("result does not match");
	}
	
	@Test
	public void testExportTempRank() {
		testData.clear();
		
		Item item1 = new Item();
		Item item2 = new Item();
		Item item3 = new Item();	
		
		item1.setName("item1");
		item2.setName("item2");
		item3.setName("item3");
		
		try {
			item1.setReorderAmount(10);
			item2.setReorderAmount(1);
			item3.setReorderAmount(12);
			
			item1.setTemperature(Constants.TEMP_NO_NEED);
			item2.setTemperature(-2);
			item3.setTemperature(20.5);
			
			testData.put(item1.getName(), item1);
			testData.put(item2.getName(), item2);
			testData.put(item3.getName(), item3);
			
			Item[] result = manifest.exportTempRank(testData);
			
			assertEquals(result.length, 3);
			assertEquals(result[0].getReorderAmount(), 1);
			assertEquals(result[1].getReorderAmount(), 12);
		} catch (InappropriateValueException e) {}		
		
	}
	
	@Test
	public void testExportordItem() {
		testData.clear();
		
		Item item1 = new Item();
		Item item2 = new Item();
		Item item3 = new Item();
		
		item1.setName("item1");
		item2.setName("item2");
		item3.setName("item3");
		
		try {
			item1.setReorderAmount(10);
			item2.setReorderAmount(1);
			item3.setReorderAmount(12);
		} catch (InappropriateValueException e) {}
		
		testData.put(item1.getName(), item1);
		testData.put(item2.getName(), item2);
		testData.put(item3.getName(), item3);
		
		item1.setTemperature(Constants.TEMP_NO_NEED);
		item2.setTemperature(Constants.TEMP_NO_NEED);
		item3.setTemperature(Constants.TEMP_NO_NEED);
		
		Item[] result = manifest.exportOrdItem(testData);
		
		assertEquals(result.length, 3);
		
		assertEquals(result[0].getReorderAmount() + result[1].getReorderAmount() + result[2].getReorderAmount(), 23);
	}
	
	@Test
	public void testExportManifest() {
		String filePath = "./testFile.csv";
		
		Item item1 = new Item();
		Item item2 = new Item();
		Item item3 = new Item();
		
		item1.setName("item1");
		item2.setName("item2");
		item3.setName("item3");
		
		try {
			item1.setReorderAmount(10);
			item2.setReorderAmount(5);
			item3.setReorderAmount(15);
			
			item1.setTemperature(-10);
			item2.setTemperature(10);
			item3.setTemperature(Constants.TEMP_NO_NEED);
		} catch (InappropriateValueException e) {
			e.printStackTrace();
		}
		
		Item[] tempArray = new Item[] {item1, item2};
		Item[] ordarnaryArray = new Item[] {item3};
		
		try {
			manifest.exportManifest(tempArray, ordarnaryArray, filePath);
			BufferedReader buf = FileTools.loadFile("./testFile.csv");
			String itemData;
			String container = null;
			while ((itemData = buf.readLine()) != null) {
				container += itemData;				
			}
			System.out.println(container);	
			
			if (!(container.contains("item1") && container.contains("item2") &&
					container.contains("item3")))
				fail("Result does not match");
				
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testLoadManifest() {
		try {
			manifest.loadManifest("./testMaterial/sales_log_wrong.csv"); // wrong format test
			fail("accepte a wrong manifest file");
			manifest.loadManifest("./testMaterial/sales_log_wrong2.csv");
			fail("accepte a wrong manifest file");
		} catch (ClassNotFoundException | IOException | DeliveryException | StockException
				| SQLException | InappropriateValueException e) {
			e.printStackTrace();
		} catch (CSVFormatException e) {}
		
		Store store = Store.getInstace();
		store.setCapital(100000);
		double currentCapital = store.getCapital();
		Stock stock = Stock.getInstace();
		HashMap<String, Item> inventory = stock.getInventory();
		
		inventory.clear();
		Item itemRice = new Item();
		Item itemChicken = new Item();
		Item itemIceCream = new Item();
		Item itemCheese = new Item();
		
		try {
			itemRice.setName("rice");
			itemRice.setManufacturingCost(1);
			itemRice.setQuantity(0);
			itemChicken.setName("chicken");
			itemChicken.setManufacturingCost(2);
			itemChicken.setQuantity(2);
			itemIceCream.setName("ice cream");
			itemIceCream.setManufacturingCost(3);
			itemIceCream.setQuantity(3);
			itemCheese.setName("cheese");
			itemCheese.setManufacturingCost(4);
			itemCheese.setQuantity(4);
		} catch (InappropriateValueException e1) {
			e1.printStackTrace();
		}
		
		inventory.put(itemRice.getName(), itemRice);
		inventory.put(itemChicken.getName(), itemChicken);
		inventory.put(itemIceCream.getName(), itemIceCream);
		inventory.put(itemCheese.getName(), itemCheese);
		
		try {
			manifest.loadManifest("./testMaterial/manifest2.csv");
			if (currentCapital - store.getCapital() <= 36)
				fail("result does not match");
		} catch (ClassNotFoundException | IOException | CSVFormatException | DeliveryException | StockException
				| SQLException | InappropriateValueException e) {
			e.printStackTrace();
		}
		
	}

}
