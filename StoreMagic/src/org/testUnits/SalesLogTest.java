package org.testUnits;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.main.controller.SalesLogController;
import org.main.exceptions.CSVFormatException;
import org.main.exceptions.InappropriateValueException;
import org.main.exceptions.StockException;
import org.main.model.Stock;
import org.main.model.Store;

public class SalesLogTest {
	
	Store store;
	Stock stock;
	int inventoryAmountBefore;
	double itemPrice;
	
	@Before
	public void resetValue() {
		stock = Stock.getInstace();
		store = Store.getInstace();
		store.setCapital(100000.0);
		inventoryAmountBefore = stock.getItemAmount();
		itemPrice = stock.getInventory().get("rice").getSellPrice();
	}
	
	/**
	 * Test the function when add a new sales log
	 * Reduce stock function and Capital reduce function
	 * 
	 * @date 09/05/2018
	 * @author Junshao Lin
	 * @throws CSVFormatException 
	 * @throws IOException 
	 */
	@Test
	public void testLog() {
		SalesLogController sales = new SalesLogController();
		try {
			sales.loadSaleLog("./testMaterial/sales_log_2.csv");
		} catch (IOException | CSVFormatException | NumberFormatException | ClassNotFoundException |
				SQLException | StockException | InappropriateValueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int currentinventoryAmount = stock.getItemAmount();
		
		if (inventoryAmountBefore - currentinventoryAmount != 2) {
			fail("Amount reduce incorrectly");
		}
		
		if (store.getCapital() - 100000.0 != itemPrice * 2) {
			fail("Capital reduce incorrectly");
		}
	}

	/**
	 * Test if insert a csv file with incorrect format
	 * 
	 * @author Junshao Lin
	 * @throws IOException 
	 */
	@Test
	public void testWrongFormatDocument() {
		try {
			SalesLogController sales = new SalesLogController();
			sales.loadSaleLog("./testMaterial/sales_log_wrong.csv");
			fail("Format detect wrong");
		} catch (CSVFormatException | IOException | NumberFormatException | ClassNotFoundException |
				SQLException | StockException | InappropriateValueException e) {			
		}
	}
}
