package org.testUnits;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.main.exceptions.InappropriateValueException;
import org.main.model.Item;
import org.main.model.OrdinaryTruck;
import org.main.model.RefrigeratedTruck;
import org.main.model.Truck;

public class TruckTest {
	static Truck truck1;
	static Truck truck2;
	/**
	 * Test function when load new item into ordinary truck
	 * @author ZIQIAN ZHANG
	 * @throws  InappropriateValueException
	 */
	@Test
	public void TestOrdinaryTruck() {
		truck1 = new OrdinaryTruck();
		Item item1 = new Item();
		Item item2 = new Item();
		Item item3 = new Item();
		try {
			item1.setName("item1");
			item2.setName("item2");
			item3.setName("item3");
			item1.setQuantity(10);
			item2.setQuantity(20);
			item3.setQuantity(30);
		}catch (InappropriateValueException e) {
			e.printStackTrace();
		}
		truck1.load(item1);
		truck1.load(item2);
		truck1.load(item3);
		if (truck1.truckCost() != 750 + 0.25 * 60) {
			fail("Ordinary truck error");
		}
	}
	/**
	 * Test function when load new item into RefrigeratedTruck
	 * @author ZIQIAN ZHANG
	 * 
	 */
	@Test
	public void TestRefrigeratedTruck() {
		truck2 = new RefrigeratedTruck();
		Item item1 = new Item();
		Item item2 = new Item();
		Item item3 = new Item();
		item1.setName("milk");
		item2.setName("cheese");
		item3.setName("ice");
		truck2.load(item1);
		truck2.load(item2);
		truck2.load(item3);
		if (truck2.truckCost() != 900.0 + 200.0 * Math.pow(0.7, (-10) / 5.0)) {
			fail("Refrigerated truck error");
		}
	}


}
